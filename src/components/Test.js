﻿import { LitElement, html } from 'lit-element';

class Test extends LitElement {
    render() {
        return html`<h1>First Test</h1>`
    }

}
customElements.define('test-view', Test);